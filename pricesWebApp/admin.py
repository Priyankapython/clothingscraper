from django.contrib import admin
from .models import *
from .adminRegisters.ProductAdmin import ProductAdmin
from .adminRegisters.ProductVariantsAdmin import ProductVariantsAdmin
from .adminRegisters.ProductPriceHistoryAdmin import ProductPriceHistoryAdmin
from .adminRegisters.ProductDescriptionHistoryAdmin import ProductDescriptionHistoryAdmin

admin.site.register(Product, ProductAdmin)
admin.site.register(ProductVariants, ProductVariantsAdmin)
admin.site.register(ProductPriceHistory, ProductPriceHistoryAdmin)
admin.site.register(ProductDescriptionHistory, ProductDescriptionHistoryAdmin)