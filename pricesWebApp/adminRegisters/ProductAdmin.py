from django.contrib import admin
from ..models import (Product,
                      ProductVariants,
                      ProductPriceHistory)

"""
The selcted product will be displayed inside shopify
"""

class ProductAdmin(admin.ModelAdmin):
    list_display = ['title', 'sku', 'source', 'selected']
    ordering = ['timestampUpdated']
    actions = ['make_selected', 'make_unselected']
    exclude = ('variants','history',)

    change_form_template = 'admin/pricesWebApp/product_template.html'
    
    def _generate_chart_data(self):
        data = {'chartData': []}

        for price in self.priceHistory:
            value1 = price.price
            value2 = price.priceMSRP
            label = price.timestampCreated
            variant = price.variant

            jsonField = None
            listId = 0

            for i, d in enumerate(data.get('chartData')):
                listId += 1
                if d.get('variant') == variant:
                    listId = i
                    jsonField = d
                    break
            else:
                jsonField = {
                    'variant': variant,
                    'prices': '',
                    'pricesMSRP': '',
                    'labels': ''
                }

                data['chartData'].append(jsonField)

            jsonField['prices'] += '%s,' % value1
            jsonField['pricesMSRP'] += '%s,' % value2
            jsonField['labels'] += '%s,' % label

            data['chartData'][listId] = jsonField

        '''
        Remove last commas from strings
        so the split would be proper
        '''
        for d in data['chartData']:
            d['prices'] = d['prices'][:-1]
            d['labels'] = d['labels'][:-1]
            d['pricesMSRP'] = d['pricesMSRP'][:-1]

        return data

    def _get_product(self, object_id):
        return Product.objects.get(id=object_id)

    def _get_latest_price_history(self):
        return ProductPriceHistory.objects.filter(sku=self.product.sku)\
                                          .order_by('-timestampCreated')

    def _get_variants(self):
        return ProductVariants.objects.filter(sku=self.product.sku)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.extra_context = {}
        self.product = self._get_product(object_id)
        variants = self._get_variants()
        self.priceHistory = self._get_latest_price_history()
        chartData = self._generate_chart_data()

        self.extra_context = {
            'variants': variants,
            'priceHistory': self.priceHistory,
            'chartData': chartData
        }
        
        return self.changeform_view(request, object_id, form_url, self.extra_context)

    def make_unselected(self, request, queryset):
        rows_updated = queryset.update(selected=False)
        if rows_updated == 1:
            message_bit = "1 product was"
        else:
            message_bit = "%s products were" % rows_updated
        self.message_user(request, "%s successfully marked as UNSELECTED." % message_bit)
    
    make_unselected.short_description = "Change products listing status to UNSELECTED"

    def make_selected(self, request, queryset):
        rows_updated = queryset.update(selected=True)
        if rows_updated == 1:
            message_bit = "1 product was"
        else:
            message_bit = "%s products were" % rows_updated
        self.message_user(request, "%s successfully marked as SELECTED." % message_bit)
    
    make_selected.short_description = "Change products listing status to SELECTED"
