from django.contrib import admin
class ProductDescriptionHistoryAdmin(admin.ModelAdmin):
    list_display = ['sku', 
    				'short_description']
    ordering = ['timestampCreated']