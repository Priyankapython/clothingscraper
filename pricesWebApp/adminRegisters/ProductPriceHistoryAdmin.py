from django.contrib import admin

class ProductPriceHistoryAdmin(admin.ModelAdmin):
    list_display = ['sku', 
    				'variant', 
    				'price', 
    				'priceMSRP', 
    				'timestampCreated']
    ordering = ['timestampCreated']