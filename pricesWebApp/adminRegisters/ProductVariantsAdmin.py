from django.contrib import admin

class ProductVariantsAdmin(admin.ModelAdmin):
    list_display = ['sku', 
                    'variant', 
                    'timestampUpdated', 
                    'timestampCreated']
    ordering = ['timestampUpdated']