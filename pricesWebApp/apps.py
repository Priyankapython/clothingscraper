from django.apps import AppConfig


class PriceswebappConfig(AppConfig):
    name = 'pricesWebApp'
    verbose_name = "Products App"
