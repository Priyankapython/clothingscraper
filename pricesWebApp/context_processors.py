from .models import Product, ProductPriceHistory, ProductVariants
from datetime import date


def main_page_statistics(request):
    prod_count = Product.objects.count() #Dashboard information
    prod_count_today = Product.objects.filter(timestampCreated__date=date.today()) #Dashboard information
    product_prices = ProductPriceHistory.objects.order_by("-id")[:10] #Table information
    variants_count = ProductVariants.objects.count()
    selected_prod_count = Product.objects.filter(selected=True).count()

    return {
        'product_count': prod_count,
        'new_prod_count': prod_count_today.count(),
        'product_prices': product_prices,
        'variants_count': variants_count,
        'selected_prod_count': selected_prod_count
    }