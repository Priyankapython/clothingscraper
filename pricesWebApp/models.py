from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from django.template.defaultfilters import truncatechars

class ProductDescriptionHistory(models.Model):
    sku = models.CharField(max_length=64)
    timestampCreated = models.DateField(auto_now_add=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return 'Sku: %s Time: %s' % (self.sku,
                                     self.timestampCreated)

    @property
    def short_description(self):
        return truncatechars(self.description, 256)

    class Meta:
        db_table = 'product_description_history'
        verbose_name = 'Products Description History'
        verbose_name_plural = 'Products Description History'
        unique_together = (('sku', 'timestampCreated'),)


class ProductPriceHistory(models.Model):
    sku = models.CharField(max_length=64)
    variant = models.CharField(max_length=64, default='')
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    priceMSRP = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    timestampCreated = models.DateField(auto_now_add=True)

    def __str__(self):
        return 'Sku: %s Variant: %s Time: %s' % (self.sku, 
                                                 self.variant,
                                                 self.timestampCreated)

    class Meta:
        db_table = 'product_price_history'
        verbose_name = 'Products Price History'
        verbose_name_plural = 'Products Price History'
        unique_together = (('sku', 'variant', 'timestampCreated'),)

class ProductVariants(models.Model):
    sku = models.CharField(max_length=64)
    variant = models.CharField(max_length=64, default='')
    images = ArrayField(models.TextField(), blank=True, default=list)
    colors = ArrayField(models.CharField(max_length=64), blank=True, default=list)
    availableSizes = ArrayField(models.CharField(max_length=64), blank=True, default=list)
    sourceUrl = models.CharField(max_length=512)
    current_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    current_priceMSRp = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    timestampUpdated = models.DateTimeField(auto_now=True)
    timestampCreated = models.DateTimeField(auto_now_add=True)
    flat_rate = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    percentage_mark_up = models.PositiveIntegerField(validators=[MinValueValidator(0), 
                                                                 MaxValueValidator(100)],
                                                     default=0)
    selected = models.BooleanField(default=True)

    def __str__(self):
        return 'Sku: %s Variant: %s Time: %s' % (self.sku, 
                                                 self.variant,
                                                 self.timestampCreated)
        
    class Meta:
        db_table = 'product_variants'
        verbose_name = 'Products Variants'
        verbose_name_plural = 'Products Variants'
        unique_together = (('sku', 'variant'),)

class Product(models.Model):
    title = models.CharField(max_length=125)
    brand = models.CharField(max_length=125)
    genders = ArrayField(models.CharField(max_length=32), blank=True, default=list)
    category = models.CharField(max_length=64)
    subcategory = models.CharField(max_length=64, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    source = models.CharField(max_length=125)
    sku = models.CharField(max_length=64)
    otherData = models.TextField(null=True, blank=True)
    selected = models.BooleanField(default=False)
    timestampCreated = models.DateTimeField(auto_now_add=True)
    timestampUpdated = models.DateTimeField(auto_now=True)
    history = models.ManyToManyField(ProductPriceHistory)
    variants = models.ManyToManyField(ProductVariants)

    def __str__(self):
        return '%s | %s' % (self.sku, self.title)

    class Meta:
        db_table = 'products'
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        unique_together = (('sku'),)
