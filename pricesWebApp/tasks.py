from __future__ import absolute_import, unicode_literals
from celery import task
from pricesWebApp.models import Product, ProductPriceHistory, ProductVariants
from time import sleep
import requests
import json

apiKey = 'apikey'
password = 'password'
storename = 'storename'
headers = {"Accept": "application/json", "Content-Type": "application/json"}

@task()
def upload_products():
    products = Product.objects.filter(selected=True)
    for product in products:
        create_product(product)
        print(product.sku)

def generate_options(colors, sizes):
    return [{
            "name": "Color",
            "values": colors
        },
        {
            "name": "Size",
            "values": sizes
        }
    ]

def generate_variant_options(variant):
    options = {}

    for c, color in enumerate(variant.colors, 1):
        options["option%s" % c] = color

    for c, size in enumerate(variant.availableSizes, 1+len(variant.colors)):
        options["option%s" % c] = size

    return options

def generate_variants(product):
    variants = list()
    colors = list()
    sizes = list()
    images = list()

    for var in product.variants.filter(selected=True):
        price = (var.current_price+var.flat_rate)
        if var.percentage_mark_up:
            price *= (1+var.percentage_mark_up/100)
        v = {
            "price": float(price),
            "sku": var.variant
        }

        for img in var.images:
            img = {"src": img}
            images.append(img)

        colors = colors + list(set(var.colors) - set(colors))
        sizes = sizes + list(set(var.availableSizes) - set(sizes))

        v.update(generate_variant_options(var))
        variants.append(v)

    options = generate_options(colors, sizes)

    return variants, options, images

def create_product(product):

    url = "https://%s:%s@%s.myshopify.com/admin/products.json" % (apiKey, password, storename)
    variants, options, images = generate_variants(product)
    payload = {
      "product": {
        "title": product.title,
        "body_html": product.description,
        "vendor": product.brand,
        "product_type": product.category,
        "variants": variants,
        "options": options,
        "images": images
      }
    }

    r = requests.post(url, json=payload,  headers=headers)
    update_product(r.text)

def update_product(prodResponse):

    data = json.loads(prodResponse)
    images = data.get('product', {}).get('images')
    variants = data.get('product', {}).get('variants')

    for variant in variants:
        url = "https://%s:%s@%s.myshopify.com/admin/variants/%s.json" % (apiKey, password, storename, variant.get('id'))
        var = ProductVariants.objects.get(variant=variant.get('sku'))
        for image in images:
            img = None
            s = image.get('src').split('/')[-1]
            s = s.split('.')[0]
            for v in var.images:
                if s in v:
                    img = v
                    break
            if img:
                d = {
                  "variant": {
                    "id": variant.get('id'),
                    "image_id": image.get('id')
                  }
                }

                r = requests.put(url, data=json.dumps(d),  headers=headers)
                break