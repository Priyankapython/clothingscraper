# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field, Item

class EcommerceItem(Item):
    title = Field()
    brand = Field()
    price = Field()
    priceMSRP = Field()
    colors = Field()
    images = Field()
    category = Field()
    subcategory = Field()
    genders = Field()
    variantId = Field()
    description = Field()
    availableSizes = Field()
    source = Field()
    sourceUrl = Field()
    sku = Field()
    otherData = Field()

    def __repr__(self):
        return repr({"title": self["title"],
                     "brand": self["brand"],
                     "source": self["source"],
                     "price": self["price"]
                    })
