# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from pricesWebApp.models import (ProductPriceHistory,
                                 Product,
                                 ProductVariants,
                                 ProductDescriptionHistory)
import logging

class DatabasePipeline(object):
    def process_item(self, item, spider):
        logging.log(logging.INFO, item)
        """
        Save deals in the database.
        This method is called for every item pipeline component.
        """

        """
        Checking if the object already exists.
        If yes, then it is updated
        If no, then it is created
        """
        try:
            product = Product.objects.get(sku=item.get("sku"))
        except Product.DoesNotExist:
            product = Product()

        try:
            variant = ProductVariants.objects.get(sku=item.get("sku"),
                                                  variant=item.get("variantId"))
        except ProductVariants.DoesNotExist:
            variant = ProductVariants()

        price = ProductPriceHistory()

        price.sku = item.get("sku")
        price.variant = item.get("variantId")
        price.price = item.get("price")
        price.priceMSRP = item.get("priceMSRP")
        try:
            price.save()
        except:
            print("%s price is duplicate" % item.get("variantId"))

        description = ProductDescriptionHistory()
        description.sku = item.get("sku")
        description.description = item.get("description")
        try:
            description.save()
        except:
            print("%s description is duplicate" % item.get("sku"))

        variant.sku = item.get("sku")
        variant.variant = item.get("variantId")
        variant.images = item.get("images")
        variant.current_price = item.get("price")
        variant.current_priceMSRP = item.get("priceMSRP")
        variant.colors = item.get("colors")
        variant.availableSizes = item.get("availableSizes")
        variant.sourceUrl = item.get("sourceUrl")

        try:
            variant.save()
        except:
            print("%s variant is duplicate" % item.get("variantId"))

        product.title = item.get("title")
        product.category = item.get("category")
        product.subcategory = item.get("subcategory")
        product.description = item.get("description")
        product.genders = item.get("genders")
        product.source = item.get("source")
        product.brand = item.get("brand")
        product.sku = item.get("sku")
        product.otherData = item.get("otherData")
        product.save()
        product.variants.add(variant)
        product.history.add(price)

        try: 
            product.save()
        except:
            print("%s product is duplicate" % item.get("sku"))

        return item