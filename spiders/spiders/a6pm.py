# -*- coding: utf-8 -*-
import scrapy
from ..items import EcommerceItem
from scrapy import Request
import json
import urllib.parse

class Xpath():
    def __init__(self):
        self.script = '//script[contains(text(), "window.__INITIAL_STATE__")]/text()'
        self.categoryUrls = '//a[text()="View all..."]/@href'
        self.nextPage = '//link[@rel="next"]/@href'

class A6pmSpider(scrapy.Spider):
    name = '6pm'
    baseUrl = 'https://www.6pm.com'
    start_urls = ['https://www.6pm.com/null/.zso?s=brandNameFacetLC/asc/productName/asc/']
    xpath = Xpath()
    productUrl = 'https://api.zcloudcat.com/v3/productBundle?'

    def setProductQueryString(self, siteId, productId):
        return {
            'productId': str(productId),
            'siteId': str(siteId),
            'includeImages': True,
            'includeSizing': True,
            'includeOosSizing': True,
            'entireProduct': True,
            'includeBrand': True,
            'includes': 'preferredSubsite',
            'autolink': 'brandProductName'
        }

    def parseProduct(self, response):
        data = json.loads(response.text)
        if data.get('statusCode') == "200":
            product = data.get("product")[0]

            brandName = product["brandName"]
            category = product["defaultCategory"]
            productType = product["defaultProductType"]
            description = product["description"]
            genders = product["genders"]
            title = product["productName"]
            for style in product.get("styles", []):
                item = EcommerceItem()
                colors = []
                images = []
                styleId = style['styleId']
                price = style["price"].replace('$', '')
                priceMSRP = style.get("originalPrice", price).replace('$', '')
                sku = style["productId"]
                colors.append(style["color"])
                url = self.baseUrl + style["productUrl"]
                availableSizes = []
                for stock in style.get("stocks", []):
                    availableSizes.append(str(stock.get('size')))

                for image in style.get("images", []):
                    imageUrl = "https://m.media-amazon.com/images/I/%s._SX480_.jpg" % image["imageId"]
                    images.append(imageUrl)

                item["title"] = title
                item["genders"] = genders
                item["brand"] = brandName
                item["price"] = float(price)
                item["priceMSRP"] = float(priceMSRP)
                item["colors"] = colors
                item["images"] = images
                item["description"] = description
                item["source"] = self.name
                item["sourceUrl"] = url
                item["sku"] = sku
                item["variantId"] = styleId
                item["availableSizes"] = availableSizes
                item["subcategory"] = category
                item["category"] = productType

                yield item

    def parseScript(self, script):
        script = script.replace('window.__INITIAL_STATE__ = ', '')
        script = script.replace('};', '}')
        return script

    def parse(self, response):
        x = response.xpath

        script = x(self.xpath.script).extract_first()
        script = self.parseScript(script)

        data = json.loads(script)

        trackingPayload = data.get('pixelServer', {}).get('data', {}).get('trackingPayload', {})
        skus = trackingPayload.get('results')
        siteId = data.get('environmentConfig', {}).get('api', {}).get('calypso', {}).get('siteId')

        for sku in skus:
            params = self.setProductQueryString(siteId, sku.get('sku'))
            url = self.productUrl + urllib.parse.urlencode(params)

            yield Request(url, self.parseProduct)

        next_page = x(self.xpath.nextPage).extract()
        if next_page:
            yield Request(self.baseUrl + next_page[0], 
                          self.parse)