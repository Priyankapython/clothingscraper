# -*- coding: utf-8 -*-
import scrapy
from scrapy import FormRequest, Request, Selector
import re
from ..items import EcommerceItem
try:
    import urllib.parse as urllib
except:
    import urllib
import json
try:
    import urlparse
except:
    import urllib.parse as urlparse

class Xpath():
    def __init__(self):
        self.categoryUrls = '//ul[@role="submenu"]/li/a'
        self.products = '//a[@itemprop="url"]/@href'
        self.category = '//h1[@class="top-center-header"]/text()'

class CoachoutletSpider(scrapy.Spider):
    name = 'coachoutlet'
    allowed_domains = ['coachoutlet.com']
    start_urls = ['http://coachoutlet.com/']
    xpath = Xpath()
    baseUrl = 'https://www.coachoutlet.com'
    productVariationUrl = 'https://www.coachoutlet.com/on/demandware.store/Sites-Coach_EOS-Site/en_US/Product-Variation?'

    def setCategoryQueryString(self, start):
        return {
            'producTilesRenderedCount': '24',
            'sz': '24',
            'start': str(start),
            'format': 'page-element',
            'viewAll': False
        }

    def setProductDetailsQueryString(self, pid, cid):
        query = {}
        query['pid'] = pid
        query['dwvar_%s_color' % pid] = cid
        query['quantity'] = '1'

        return query

    def parseAttributes(self, product):
        attributes = []
        values = product.get('values')
        for value in values:
            if value.get('selected') == True:
                attributes.append(value.get('displayValue'))

        return attributes

    def parseProduct(self, response):
        item = EcommerceItem()
        colors = []
        sizes = []
        images = []
        data = json.loads(response.text)
        product = data.get('product', {})
        title = product.get('productName')
        price = product.get('price', {}).get('sales', {}).get('value')
        try:
            priceMSRP = product.get('price', {}).get('list', {}).get('value')
        except:
            priceMSRP = None
        description = product.get('longDescription')
        images.append(product.get('productImage'))
        variations = product.get('variationAttributes', [])
        for variation in variations:
            attributeId = variation.get('attributeId')
            if attributeId == "color":
                colors = self.parseAttributes(variation)
            if attributeId == 'size':
                sizes = self.parseAttributes(variation)

        productUrl = product.get('selectedProductUrl')

        item['title'] = title
        item['price'] = price
        item['priceMSRP'] = priceMSRP
        item['colors'] = str(colors)
        item['images'] = str(images)
        item['availableSizes'] = str(sizes)
        item['description'] = description
        item['source'] = self.name
        item['brand'] = 'coach'
        item['sourceUrl'] = response.url
        item['sku'] = response.meta['sku']
        item['category'] = response.meta['category'].strip()

        yield item

    def parseCategory(self, response):
        xpath = self.xpath
        products = response.xpath(xpath.products).extract()

        for product in products:
            parsed = urlparse.urlparse(product)

            colorCode = urlparse.parse_qs(parsed.query)['dwvar_color'][0]
            product = product[:product.find('?')]
            productId = product.rsplit('/', 1)[1].replace('.html', '')
            sku = "%s%s" % (productId, colorCode)
            query = self.setProductDetailsQueryString(productId, colorCode)

            url = self.productVariationUrl + urllib.urlencode(query)
            yield Request(url, 
                          self.parseProduct, 
                          meta={'sku': sku,
                                'category': response.meta['category']})

        if len(products) == 24:
            start = response.meta['start']
            nextStart = start+24
            url = response.url.replace('&start=%s' % start, '&start=%s' % nextStart)
            yield Request(url, 
                          self.parseCategory,
                          meta={'start': nextStart,
                                'category': response.meta['category']})

    def parse(self, response):
        categoryUrls = response.xpath(self.xpath.categoryUrls).extract()[1:]
        for category in categoryUrls:
            sel = Selector(text=category)
            text = sel.xpath('//text()').extract_first()
            url = sel.xpath('//@href').extract_first()

            if text != "View All":
                params = self.setCategoryQueryString(0)
                catUrl = url + "?" + urllib.urlencode(params)
                yield Request(catUrl, 
                              self.parseCategory,
                              meta={'start': 0,
                                    'category': text})