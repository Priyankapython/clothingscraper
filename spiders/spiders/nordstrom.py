# -*- coding: utf-8 -*-
import scrapy
import json
from scrapy import Request
from ..items import EcommerceItem

class Xpath():
    def __init__(self):
        self.script = '//script[contains(text(), "traceContext")]'
        self.products = '//a[contains(@data-element, "product-module-media-link")]/@href'
        self.nextPage = '//a[@data-element="page-arrow-page-next-link"]/@href'

class NordstromSpider(scrapy.Spider):
    name = 'nordstrom'
    allowed_domains = ['nordstrom.com']
    start_urls = ['https://shop.nordstrom.com/c/all-women?breadcrumb=Home%2FWomen%2FAll%20Women&top=10&offset=1&page=950&sort=Boosted']
    baseUrl = 'https://shop.nordstrom.com'
    requestUrl = 'https://shop.nordstrom.com/api/style/'

    xpath = Xpath()

    def parseScript(self ,script):
        script = script.split('});')[0] + '}'
        script = script.split('{renderer(')[1]
        return script

    def parseProduct(self, response):
        data = json.loads(response.text)
        breadcrumb = data.get('breadcrumbs')
        
        title = data.get('productName')
        prodId = data.get('id')

        brand = data.get('brand', {}).get('brandName')
        variantIds = data.get('skus', {}).get('byId')
        colors = data.get('filters', {}).get('color', {}).get('byId')
        imagesStyle = data.get('styleMedia', {}).get('byId')
        fitAndSize = data.get('fitAndSize', {}).get('contextualSizeDetail')
        detailsAndCare = data.get('sellingStatement')
        features = data.get('features')
        gender = data.get('gender')
        description = data.get('description')

        for variant in variantIds:
            item = EcommerceItem()
            images = []
            colorsArray = []
            genders = []
            availableSizes = []
            var = variantIds.get(variant)
            price = var.get('displayPrice')
            priceMSRP = var.get('displayOriginalPrice')
            colorID = var.get('colorId')
            colorsArray.append(colors[str(colorID)].get('value'))
            size = var.get('sizeId')
            availableSizes.append(size)
            genders.append(gender)

            for id in colors[str(colorID)].get('styleMediaIds'):
                imageUrl = imagesStyle.get(str(id)).get('imageMediaUri', {}).get('largeDesktop')
                images.append(imageUrl)

            otherData = {}
            otherData['fitAndSize'] = fitAndSize
            otherData['detailsAndCare'] = detailsAndCare
            otherData['features'] = features

            item['description'] = description
            item['title'] = title
            item['brand'] = brand
            try:
                item['category'] = breadcrumb[-3].get('text')
                item['subcategory'] = breadcrumb[-2].get('text')
            except:
                pass
            item['colors'] = colorsArray
            item['images'] = images
            item['genders'] = genders
            item['price'] = float(price.replace('$', ''))
            item['priceMSRP'] = float(priceMSRP.replace('$', ''))
            item['sku'] = prodId
            item['variantId'] = variant
            item['availableSizes'] = availableSizes
            item['source'] = self.name
            item['sourceUrl'] = response.url
            item['otherData'] = otherData

            yield item

    def parse(self, response):
        headers = {'NordApiVersion': '6'}
        xpath = self.xpath

        script = response.xpath(xpath.script).extract_first()
        script = self.parseScript(script)
        data = json.loads(script)

        productIds = data.get('productResults', {}).get('query', {}).get('resultProductIds', [])
        for product in productIds:
            yield Request(self.requestUrl + str(product), 
                          self.parseProduct,
                          headers=headers)

        nextPage = response.xpath(xpath.nextPage).extract()
        if nextPage:
            nextUrl = response.urljoin(nextPage[0])
            yield Request(nextUrl, self.parse)
